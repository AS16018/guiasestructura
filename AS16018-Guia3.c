#include <stdio.h>

int main() {

    int num1, num2, opcion, valor1, valor2, resultado;
    int *p1;
	int *p2;
	int *p3;

    printf("Ingrese el primer numero entero\n");
    scanf("%d", &num1);
    printf("Ingrese el segundo numero entero\n");
    scanf("%d", &num2);
    
    p1 = &num1;
	p2 = &num2;
	p3 = &resultado;
	
	do {
        printf("\n------------MENU-------------\n");
        printf("1-Ingrese otros numeros enteros \n");
        printf("2-calcular la suma atravez de punteros\n");
        printf("3-calcular la resta atravez de punteros\n");
        printf("4-imprimir la direccion de memoria de cada valor\n");
        scanf("%d", &opcion);
        
        switch (opcion) {

            case 1:

                printf("Ingrese el nuevo valor del primer numero\n");
                scanf("%d", &valor1);
                printf("Ingrese el nuevo valor del segundo numero\n");
                scanf("%d", &valor2);
                
                num1=valor1;
                num2=valor2;
                
                break;

            case 2:
                *p3=(*p1)+(*p2);
                printf("La suma es:\n%d",*p3);
                break;
            case 3:
                
                *p3=(*p1)-(*p2);
                printf("La resta es:\n%d",*p3);
                break;
            case 4:
                
                printf("la direccion del primer numero es: %d\n",p1);
                printf("la direccion del segundo numero es: %d\n",p2);
                
                
                break;



        }
        
        
	}while (opcion > 0 && opcion < 5);
    


return 0;
}
